package com.demoapp.alcodesonboard.fragments;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.demoapp.alcodesonboard.BuildConfig;
import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.activities.ProfileImageActivity;
import com.demoapp.alcodesonboard.database.entities.MyFriend;
import com.demoapp.alcodesonboard.gsonmodels.MyFriendsModel;
import com.demoapp.alcodesonboard.gsonmodels.UserDataModel;
import com.demoapp.alcodesonboard.gsonmodels.UserModel;
import com.demoapp.alcodesonboard.utils.NetworkHelper;
import com.demoapp.alcodesonboard.viewmodels.MyFriendsViewModel.MyFriendsViewModel;
import com.demoapp.alcodesonboard.viewmodels.MyFriendsViewModel.MyFriendsViewModelFactory;
import com.google.gson.GsonBuilder;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import timber.log.Timber;

public class MyFriendDetailFragment extends Fragment {

    public static final String TAG = MyFriendDetailFragment.class.getSimpleName();

    private static final String ARG_LONG_MY_FRIEND_ID = "ARG_LONG_MY_FRIEND_ID";

    private final int REQUEST_CODE_PROFILE_IMAGE = 300;

    @BindView(R.id.circleimageview_profile_detail_image)
    protected CircleImageView mCircleImageView;

    @BindView(R.id.textview_firstname)
    protected TextView mTextViewFirstName;

    @BindView(R.id.textview_lastname)
    protected TextView mTextViewLastName;

    @BindView(R.id.textview_email)
    protected TextView mTextViewEmail;

    @BindView(R.id.progress_bar_detail_loading)
    protected ProgressBar mProgressBarDetailLoading;

    private Unbinder mUnbinder;
    private long mMyFriendId = 0L;
    private MyFriendsViewModel mViewModel;

    private String mImagePath = "";
    private String mEmail = "";

    public MyFriendDetailFragment(){
    }

    public static MyFriendDetailFragment newInstance (long id){
        Bundle args = new Bundle();
        args.putLong(ARG_LONG_MY_FRIEND_ID, id);

        MyFriendDetailFragment fragment = new MyFriendDetailFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_friend_detail, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //Check arguments pass from previous page
        Bundle args = getArguments();

        if(args != null){
            mMyFriendId = args.getLong(ARG_LONG_MY_FRIEND_ID, 0);
        }

        initView();
    }

    @Override
    public void onDestroy() {
        if(mUnbinder != null){
            mUnbinder.unbind();
        }

        super.onDestroy();
    }

    private boolean isConnected() {
        ConnectivityManager cm =
                (ConnectivityManager) getActivity().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    private void initView(){

        if(mMyFriendId > 0){

            /* GreenDAO
            mViewModel = new ViewModelProvider(this, new MyFriendsViewModelFactory(getActivity().getApplication())).get(MyFriendsViewModel.class);
            mViewModel.loadMySingleFriendData(mMyFriendId);
            MyFriend myFriend = mViewModel.getMySingleFriendData();

            if(myFriend != null){
                Glide.with(getActivity())
                        .load(myFriend.getAvatar())
                        .into(mCircleImageView);
                mTextViewFirstName.setText("F. Name: " + myFriend.getFirstname());
                mTextViewLastName.setText("L. Name: " + myFriend.getLastname());
                mTextViewEmail.setText("Email: " + myFriend.getEmail());


            }else{
                // Record not found.
                Toast.makeText(getActivity(), "Note not found.", Toast.LENGTH_SHORT).show();
                getActivity().finish();
            }
            */

            if(!isConnected()){
                new MaterialDialog.Builder(getActivity())
                        .title("Error")
                        .content("You are currently offline. Check your internet connection")
                        .positiveText("Understood")
                        .show();

                return;
            }

            String url = BuildConfig.BASE_API_URL + "users/" + mMyFriendId;
            mProgressBarDetailLoading.setVisibility(View.VISIBLE);

            StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    UserDataModel responseModel = new GsonBuilder().create().fromJson(response, UserDataModel.class);

                    if(responseModel != null){
                        mImagePath =responseModel.data.avatar;
                        mEmail = responseModel.data.email;

                        Glide.with(getActivity())
                                .load(mImagePath)
                                .into(mCircleImageView);
                        mTextViewFirstName.setText("F. Name: " + responseModel.data.first_name);
                        mTextViewLastName.setText("L. Name: " + responseModel.data.last_name);
                        mTextViewEmail.setText("Email: " + mEmail);

                    }else{
                        // Record not found.
                        Toast.makeText(getActivity(), "Note not found.", Toast.LENGTH_SHORT).show();
                        getActivity().finish();
                    }
                    mProgressBarDetailLoading.setVisibility(View.GONE);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Timber.e("Error: " + error.toString());
                    mProgressBarDetailLoading.setVisibility(View.GONE);
                }
            });

            NetworkHelper.getRequestQueueInstance(getActivity()).add(stringRequest);
        }
    }

    @OnClick(R.id.textview_email)
    protected void startEmailSoftware(){
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("plain/text");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[] {mEmail.toString()});
        intent.putExtra(Intent.EXTRA_SUBJECT, "");
        intent.putExtra(Intent.EXTRA_TEXT, "");
        Intent mailer = Intent.createChooser(intent,null);
        startActivity(mailer);
    }

    @OnClick(R.id.circleimageview_profile_detail_image)
    protected void startProfileImageActivity(){
        Intent intent = new Intent(getActivity(), ProfileImageActivity.class);
        intent.putExtra(ProfileImageActivity.EXTRA_STRING_PROFILE_IMAGE_PATH, mImagePath);

        startActivityForResult(intent, REQUEST_CODE_PROFILE_IMAGE);
    }
}
