package com.demoapp.alcodesonboard.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.demoapp.alcodesonboard.BuildConfig;
import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.activities.MyFriendDetailActivity;
import com.demoapp.alcodesonboard.activities.ProfileImageActivity;
import com.demoapp.alcodesonboard.adapters.MyFriendsAdapter;
import com.demoapp.alcodesonboard.database.entities.MyFriend;
import com.demoapp.alcodesonboard.gsonmodels.MyFriendsModel;
import com.demoapp.alcodesonboard.gsonmodels.UserModel;
import com.demoapp.alcodesonboard.utils.NetworkHelper;
import com.demoapp.alcodesonboard.viewmodels.MyFriendsViewModel.MyFriendsViewModel;
import com.demoapp.alcodesonboard.viewmodels.MyFriendsViewModel.MyFriendsViewModelFactory;
import com.google.gson.GsonBuilder;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import timber.log.Timber;

public class MyFriendsFragment extends Fragment implements MyFriendsAdapter.Callbacks{

    public static final String TAG = MyFriendsFragment.class.getSimpleName();

    @BindView(R.id.recyclerview_my_friends)
    protected RecyclerView mRecyclerViewMyFriends;

    @BindView(R.id.textview_no_data)
    protected TextView mTextViewNoData;

    @BindView(R.id.progress_bar_list_loading)
    protected ProgressBar mProgressBarListLoading;

    private Unbinder mUnbiner;
    private MyFriendsAdapter mAdapter;
    private MyFriendsViewModel mViewModel;

    private List<MyFriendsAdapter.DataHolder> dataHolders = new ArrayList<>();

    private boolean isScrolling = false;
    private int currentItems, totalItems, scrollOutItems;
    private int friendsDataNextPageNumber, totalPagesInAPI;

    public MyFriendsFragment(){

    }

    public static MyFriendsFragment newInstance() {return new MyFriendsFragment();}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_friends, container, false);

        mUnbiner = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        if(!isConnected()){
            new MaterialDialog.Builder(getActivity())
                    .title("Error")
                    .content("You are currently offline. Check your internet connection")
                    .positiveText("Understood")
                    .show();

            return;
        }

        friendsDataNextPageNumber = 1;
        callFriendsAPI();
        initView();
        //initViewModel();
    }

    @Override
    public void onDestroy() {
        if(mUnbiner != null){
            mUnbiner.unbind();
        }

        super.onDestroy();
    }

    @Override
    public void onListItemClicked(MyFriendsAdapter.DataHolder data) {
        Intent intent = new Intent(getActivity(), MyFriendDetailActivity.class);
        intent.putExtra(MyFriendDetailActivity.EXTRA_LONG_MY_FRIEND_ID, data.id);

        startActivity(intent);
    }

    @Override
    public void onImageViewClicked(String imagePath) {
        Intent intent = new Intent(getActivity(), ProfileImageActivity.class);
        intent.putExtra(ProfileImageActivity.EXTRA_STRING_PROFILE_IMAGE_PATH, imagePath);

        startActivity(intent);
    }

    private void callFriendsAPI(){
        String url = BuildConfig.BASE_API_URL + "users?page=" + friendsDataNextPageNumber;

        if(mProgressBarListLoading.getVisibility() != View.VISIBLE){
            mProgressBarListLoading.setVisibility(View.VISIBLE);
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                //Convert JSON string to JAVA object
                MyFriendsModel responseModel = new GsonBuilder().create().fromJson(response, MyFriendsModel.class);

                totalPagesInAPI = Integer.parseInt(responseModel.total_pages);

                /* GreenDAO
                if(mViewModel.getMyFriendsAdapterListLiveData() == null){
                    for(UserModel userModel: responseModel.data){
                        //Create record.
                        mViewModel.addFriend(userModel.id, userModel.first_name, userModel.last_name, userModel.email, userModel.avatar);
                    }
                }
                */

                for(UserModel userModel: responseModel.data){
                    MyFriendsAdapter.DataHolder dataHolder = new MyFriendsAdapter.DataHolder();
                    dataHolder.id = userModel.id;
                    dataHolder.firstname = userModel.first_name;
                    dataHolder.lastname = userModel.last_name;
                    dataHolder.email = userModel.email;
                    dataHolder.avatar = userModel.avatar;
                    dataHolders.add(dataHolder);
                }

                mAdapter.setData(dataHolders);
                mAdapter.notifyDataSetChanged();

                mProgressBarListLoading.setVisibility(View.GONE);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Timber.e("Error: " + error.toString());
                mProgressBarListLoading.setVisibility(View.GONE);
            }
        });

        NetworkHelper.getRequestQueueInstance(getActivity()).add(stringRequest);
    }

    private boolean isConnected() {
        ConnectivityManager cm =
                (ConnectivityManager) getActivity().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    private void initView(){
        mAdapter = new MyFriendsAdapter();
        mAdapter.setCallBacks(this);

        mRecyclerViewMyFriends.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        mRecyclerViewMyFriends.setHasFixedSize(true);
        mRecyclerViewMyFriends.setAdapter(mAdapter);

        mRecyclerViewMyFriends.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL){
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                // Number of items that you can see in phone
                currentItems = recyclerView.getLayoutManager().getChildCount();

                //Number of items that totally have
                totalItems = recyclerView.getLayoutManager().getItemCount();

                //Number of items that are invisible after scrolled
                scrollOutItems = ((LinearLayoutManager)recyclerView.getLayoutManager()).findFirstVisibleItemPosition();

                if(isScrolling && (currentItems + scrollOutItems == totalItems)){
                    isScrolling = false;

                    if(friendsDataNextPageNumber < totalPagesInAPI){
                        friendsDataNextPageNumber++;
                        mProgressBarListLoading.setVisibility(View.VISIBLE);
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                callFriendsAPI();
                            }
                        }, 3000);
                    }

                }
            }
        });
    }

    /*
    private void initViewModel(){
        mViewModel = new ViewModelProvider(this, new MyFriendsViewModelFactory(getActivity().getApplication())).get(MyFriendsViewModel.class);
        mViewModel.getMyFriendsAdapterListLiveData().observe(getViewLifecycleOwner(), new Observer<List<MyFriendsAdapter.DataHolder>>() {
            @Override
            public void onChanged(List<MyFriendsAdapter.DataHolder> dataHolders) {
                mAdapter.setData(dataHolders);
                mAdapter.notifyDataSetChanged();

                if(dataHolders.isEmpty()){
                    mTextViewNoData.setVisibility(View.VISIBLE);
                    mRecyclerViewMyFriends.setVisibility(View.GONE);
                }else{
                    mTextViewNoData.setVisibility(View.GONE);
                    mRecyclerViewMyFriends.setVisibility(View.VISIBLE);
                }
            }
        });

        //Load data into adapter
        mViewModel.loadMyFriendsAdapterList();
    }
    */
}
