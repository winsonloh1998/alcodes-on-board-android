package com.demoapp.alcodesonboard.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.demoapp.alcodesonboard.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileImageFragment extends Fragment {

    public static final String TAG = ProfileImageFragment.class.getSimpleName();

    private static final String ARG_LONG_PROFILE_IMAGE_PATH = "ARG_LONG_PROFILE_IMAGE_PATH";

    @BindView(R.id.circleimageview_big_profile_image)
    protected CircleImageView circleImageViewBigProfileImage;

    private Unbinder mUnbinder;

    private String mProfileImagePath = "";

    public ProfileImageFragment(){
    }

    public static ProfileImageFragment newInstance(String imagePath){
        Bundle args = new Bundle();
        args.putString(ARG_LONG_PROFILE_IMAGE_PATH, imagePath);

        ProfileImageFragment fragment = new ProfileImageFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile_image, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //Check Arguments pass from previous page
        Bundle args = getArguments();

        if(args != null){
            mProfileImagePath = args.getString(ARG_LONG_PROFILE_IMAGE_PATH,"");
        }

        initView();
    }

    @Override
    public void onDestroy() {
        if(mUnbinder != null){
            mUnbinder.unbind();
        }

        super.onDestroy();
    }

    private void initView(){
        if(!mProfileImagePath.isEmpty()){
            Glide.with(getActivity())
                    .load(mProfileImagePath)
                    .into(circleImageViewBigProfileImage);
        }
    }
}
