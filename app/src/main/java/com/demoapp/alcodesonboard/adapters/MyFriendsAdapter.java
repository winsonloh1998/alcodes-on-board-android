package com.demoapp.alcodesonboard.adapters;

import android.content.Intent;
import android.net.Uri;
import android.provider.ContactsContract;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.activities.MyFriendsActivity;
import com.demoapp.alcodesonboard.fragments.MyFriendsFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class MyFriendsAdapter extends RecyclerView.Adapter<MyFriendsAdapter.ViewHolder> {

    private List<DataHolder> mData = new ArrayList<>();
    private Callbacks mCallbacks;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_friends, parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position){
        holder.bindTo(mData.get(position), mCallbacks);
    }

    @Override
    public int getItemCount(){return mData.size();}

    public void setData(List<DataHolder> data){
        if(data == null){
            mData = new ArrayList<>();
        }else{
            mData = data;
        }
    }

    public void setCallBacks(Callbacks callbacks){mCallbacks = callbacks; }

    public static class DataHolder{
        public Long id;
        public String avatar;
        public String firstname;
        public String lastname;
        public String email;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.relativelayout_root)
        public RelativeLayout root;

        @BindView(R.id.circleimageview_profile_image)
        public CircleImageView imageView;

        @BindView(R.id.textview_name)
        public TextView name;

        @BindView(R.id.textview_email)
        public TextView email;

        public ViewHolder (@NonNull View itemView){
            super(itemView);

            ButterKnife.bind(this,itemView);
        }

        public void bindTo(DataHolder data, Callbacks callbacks){
            if(data != null){
                Glide.with(root)
                        .load(data.avatar)
                        .into(imageView);
                name.setText(data.lastname + " " + data.firstname);
                email.setText(data.email);

                if(callbacks != null){
                    root.setOnClickListener(new View.OnClickListener(){
                        @Override
                        public void onClick(View v) {
                            callbacks.onListItemClicked(data);
                        }
                    });

                    imageView.setOnClickListener(new View.OnClickListener(){
                        @Override
                        public void onClick(View v) {
                            callbacks.onImageViewClicked(data.avatar);
                        }
                    });
                }
            }
        }
    }

    public interface Callbacks {

        void onListItemClicked(DataHolder data);

        void onImageViewClicked(String imagePath);
    }
}
