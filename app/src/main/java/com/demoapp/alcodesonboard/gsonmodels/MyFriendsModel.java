package com.demoapp.alcodesonboard.gsonmodels;

import androidx.annotation.NonNull;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MyFriendsModel {

    public String page;
    public String per_page;
    public String total;
    public String total_pages;
    public List<UserModel> data;
}
