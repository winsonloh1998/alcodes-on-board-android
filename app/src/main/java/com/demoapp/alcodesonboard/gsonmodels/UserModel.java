package com.demoapp.alcodesonboard.gsonmodels;

public class UserModel {

    public long id;
    public String email;
    public String first_name;
    public String last_name;
    public String avatar;
}
