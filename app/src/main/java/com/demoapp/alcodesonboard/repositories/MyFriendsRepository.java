package com.demoapp.alcodesonboard.repositories;

import android.content.Context;
import android.provider.ContactsContract;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.demoapp.alcodesonboard.adapters.MyFriendsAdapter;
import com.demoapp.alcodesonboard.database.entities.MyFriend;
import com.demoapp.alcodesonboard.utils.DatabaseHelper;

import java.util.ArrayList;
import java.util.List;

public class MyFriendsRepository {

    private static MyFriendsRepository mInstance;

    private MutableLiveData<List<MyFriendsAdapter.DataHolder>> mMyFriendsAdapterListLiveData = new MutableLiveData<>();

    private MyFriend mMySingleFriendData = new MyFriend();

    public static MyFriendsRepository getInstance(){
        if(mInstance == null){
            synchronized (MyFriendsRepository.class){
                mInstance = new MyFriendsRepository();
            }
        }

        return mInstance;
    }

    public MyFriendsRepository(){
    }

    public LiveData<List<MyFriendsAdapter.DataHolder>> getMyFriendsAdapterListLiveData(){
        return mMyFriendsAdapterListLiveData;
    }

    public void loadMyFriendsAdapterList (Context context){
        List<MyFriendsAdapter.DataHolder> dataHolders = new ArrayList<>();
        List<MyFriend> records = DatabaseHelper.getInstance(context)
                .getMyFriendDao()
                .loadAll();

        if(records != null){
            for(MyFriend myFriend: records){
                MyFriendsAdapter.DataHolder dataHolder = new MyFriendsAdapter.DataHolder();
                dataHolder.id = myFriend.getId();
                dataHolder.avatar = myFriend.getAvatar();
                dataHolder.firstname = myFriend.getFirstname();
                dataHolder.lastname = myFriend.getLastname();
                dataHolder.email = myFriend.getEmail();

                dataHolders.add(dataHolder);
            }
        }

        mMyFriendsAdapterListLiveData.setValue(dataHolders);
    }

    public MyFriend getMySingleFriendData(){ return mMySingleFriendData; }

    public void loadMySingleFriendData(Context context, Long id){
        MyFriend myFriend = DatabaseHelper.getInstance(context)
                .getMyFriendDao()
                .load(id);

        mMySingleFriendData = myFriend;
    }

    public void addFriend(Context context, Long id, String firstname, String lastname, String email, String avatar){
        //Create new record
        MyFriend myFriend = new MyFriend();
        myFriend.setId(id);
        myFriend.setFirstname(firstname);
        myFriend.setLastname(lastname);
        myFriend.setEmail(email);
        myFriend.setAvatar(avatar);

        //Add record to database
        DatabaseHelper.getInstance(context)
                .getMyFriendDao()
                .insert(myFriend);
    }
}
