package com.demoapp.alcodesonboard.viewmodels.MyFriendsViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.demoapp.alcodesonboard.adapters.MyFriendsAdapter;
import com.demoapp.alcodesonboard.database.entities.MyFriend;
import com.demoapp.alcodesonboard.repositories.MyFriendsRepository;

import java.util.List;

public class MyFriendsViewModel extends AndroidViewModel {

    private MyFriendsRepository mMyFriendsRepository;

    public MyFriendsViewModel (@NonNull Application application){
        super(application);

        mMyFriendsRepository = MyFriendsRepository.getInstance();
    }

    public LiveData<List<MyFriendsAdapter.DataHolder>> getMyFriendsAdapterListLiveData(){
        return mMyFriendsRepository.getMyFriendsAdapterListLiveData();
    }

    public MyFriend getMySingleFriendData(){ return mMyFriendsRepository.getMySingleFriendData(); }

    public void loadMyFriendsAdapterList(){
        mMyFriendsRepository.loadMyFriendsAdapterList(getApplication());
    }

    public void loadMySingleFriendData(Long id){
        mMyFriendsRepository.loadMySingleFriendData(getApplication(),id);
    }

    public void addFriend(Long id, String firstname, String lastname, String email, String avatar){
        mMyFriendsRepository.addFriend(getApplication(), id, firstname, lastname, email, avatar);
    }
}
