package com.demoapp.alcodesonboard.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.fragments.ProfileImageFragment;

import butterknife.ButterKnife;

public class ProfileImageActivity extends AppCompatActivity {

    public static final String EXTRA_STRING_PROFILE_IMAGE_PATH = "EXTRA_STRING_PROFILE_IMAGE_PATH";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_profile_image);

        ButterKnife.bind(this);

        FragmentManager fragmentManager = getSupportFragmentManager();

        if(fragmentManager.findFragmentByTag(ProfileImageFragment.TAG) == null){
            //Init fragment
            Intent extra = getIntent();
            String imagePath = "";

            if(extra != null){
                imagePath = extra.getStringExtra(EXTRA_STRING_PROFILE_IMAGE_PATH);
            }

            fragmentManager.beginTransaction()
                    .replace(R.id.framelayout_fragment_holder,ProfileImageFragment.newInstance(imagePath), ProfileImageFragment.TAG)
                    .commit();
        }
    }
}
