package com.demoapp.alcodesonboard.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.database.entities.MyFriend;
import com.demoapp.alcodesonboard.fragments.MyFriendDetailFragment;
import com.demoapp.alcodesonboard.fragments.MyFriendsFragment;

import butterknife.ButterKnife;

public class MyFriendDetailActivity extends AppCompatActivity {

    public static final String EXTRA_LONG_MY_FRIEND_ID = "EXTRA_LONG_MY_FRIEND_ID";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_my_friend_detail);

        ButterKnife.bind(this);

        FragmentManager fragmentManager = getSupportFragmentManager();

        if(fragmentManager.findFragmentByTag(MyFriendDetailFragment.TAG) == null){
            //Init fragment
            Intent extra = getIntent();
            long friendId = 0;

            if(extra != null){
                friendId = extra.getLongExtra(EXTRA_LONG_MY_FRIEND_ID, 0);
            }

            fragmentManager.beginTransaction()
                    .replace(R.id.framelayout_fragment_holder, MyFriendDetailFragment.newInstance(friendId), MyFriendDetailFragment.TAG)
                    .commit();
        }
    }
}
