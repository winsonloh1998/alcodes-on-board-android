package com.demoapp.alcodesonboard.activities;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.fragments.MyFriendsFragment;

import butterknife.ButterKnife;

public class MyFriendsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_my_friends);

        ButterKnife.bind(this);

        FragmentManager fragmentManager= getSupportFragmentManager();

        if(fragmentManager.findFragmentByTag(MyFriendsFragment.TAG) == null){
            //Init Fragment
            fragmentManager.beginTransaction()
                    .replace(R.id.framelayout_fragment_holder, MyFriendsFragment.newInstance(), MyFriendsFragment.TAG)
                    .commit();
        }
    }
}
